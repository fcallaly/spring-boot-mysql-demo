# Useful command line snippets

### Starting an app
Once the pipeline is created you can start the apps with:
oc new-app --env MYSQL_ROOT_PASSWORD=root markets-mysql
oc new-app markets-app
oc expose svc/markets-app

### Get url for webhook
oc describe bc/tpfrank-markets-pipeline

### change the default empty-dir volume to a persistent claim
oc set volume dc/markets-mysql --add --overwrite --name=markets-mysql-volume-1 --type=persistentVolumeClaim --claim-name=mysql-claim0

### Config to allow openshift read from dockerreg:
In origin container:
vi /var/lib/origin/openshift.local.config/master/master-config.yaml

imagePolicyConfig:
  allowedRegistriesForImport:
  - domainName: 'dockerreg.conygre.com:5000'

On host:
docker restart origin
