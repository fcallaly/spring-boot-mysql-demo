package com.neueda.training.market.database;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleAbstractTypeResolver;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.neueda.training.market.model.DtoFactory;
import com.neueda.training.market.model.Market;

/**
 * Factory functions to create DTO objects for database access
 * 
 * @author Neueda
 *
 */
@Component
public class DatabaseFactory implements DtoFactory {
	
	public DatabaseFactory(@Autowired ObjectMapper mapper) {
		SimpleModule module = new SimpleModule("CustomModel", Version.unknownVersion());

		SimpleAbstractTypeResolver resolver = new SimpleAbstractTypeResolver();
		resolver.addMapping(Market.class, MarketDto.class);
		module.setAbstractTypes(resolver);
		mapper.registerModule(module);	
	}
	
	@JsonCreator
	@Override
	public Market createMarket() {
		return new MarketDto();
	}

	@Override
	public Market createMarket(String ticker) {
		return new MarketDto(ticker);
	}
}
