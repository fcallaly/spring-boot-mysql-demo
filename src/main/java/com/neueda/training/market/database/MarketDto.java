
package com.neueda.training.market.database;

import com.neueda.training.market.model.Market;

public class MarketDto implements Market {

    private String ticker;

    public MarketDto() {}

    public MarketDto(String ticker) {
		this.setTicker(ticker);
	}

	@Override
    public String toString() {
        return String.format("Market: {%s}",
                             ticker);
    }

	/* (non-Javadoc)
	 * @see com.neueda.training.country.model.Market#getTicker()
	 */
	@Override
	public String getTicker() {
		return ticker;
	}

	/* (non-Javadoc)
	 * @see com.neueda.training.country.model.Market#setTicker(java.lang.String)
	 */
	@Override
	public void setTicker(String ticker) {
		this.ticker = ticker;
	}
}
