package com.neueda.training.market.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.neueda.training.market.database.MarketDto;
import com.neueda.training.market.model.Market;
import com.neueda.training.market.model.MarketDao;
import com.neueda.training.market.rules.Model;

@Component
public class MySqlMarketDao implements MarketDao {

    @Autowired
    private JdbcTemplate tpl;

    @Override
    public int rowCount() {
        return tpl.queryForObject("select count(*) from Markets", Integer.class);
    }

    @Override
    public List<MarketDto> findAll() {
        return this.tpl.query("select ticker from Markets order by ticker", new MarketMapper());
    }

    @Override
    public Market findByTicker(String ticker) {
        List<MarketDto> markets = this.tpl.query("select ticker from Markets where ticker = ?", new Object[] { ticker },
                new MarketMapper());
        return Model.validateFindUnique(markets, ticker, "market");
    }

    @Override
    public Market create(Market market) {
        Model.validateMarket(market);

        this.tpl.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement("insert into Markets (ticker) values (?)");
                ps.setString(1, market.getTicker());
                return ps;
            }
        });
        return findByTicker(market.getTicker());
    }

    private static final class MarketMapper implements RowMapper<MarketDto> {
        public MarketDto mapRow(ResultSet rs, int rowNum) throws SQLException {
            MarketDto market = new MarketDto();
            market.setTicker(rs.getString("ticker"));
            return market;
        }
    }

}
